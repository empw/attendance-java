/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.id.attendance;

/**
 *
 * @author emp_t
 */
public class Student {
    public String student_id;
    public String name;
    public String email;
    public String password;
    
    public void setStudentId(String newStudentId) {
        this.student_id = newStudentId;
    }
    
    public void setName(String firstName, String lastName) {
        this.name = firstName + " " + lastName;
    }
    
    public String getStudentId(){
        return this.student_id;
    }

    public String getName(){
        return this.name;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public String getPassword(){
        return this.password;
    }
}
