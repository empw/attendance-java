/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.id.attendance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author emp_t
 */
public class Connections {
    
    private Connection connection;

    public Connection getConnection() {
        return connection;
    }
    
    public void dbConnection() {
        // Chek Driver  
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); //<-- Drive Name  

            // Chek Database  
            try {
                String url, user, password;
                url = "jdbc:mysql://localhost:3306/java_attendance"; //Address Db  
                user = "root";
                password = "";
                connection = DriverManager.getConnection(url, user, password);

                System.out.println("Connection Success");
            } catch (SQLException se) {
                JOptionPane.showMessageDialog(null, "Connection Failed! " + se);
                System.exit(0);
            }
        } catch (ClassNotFoundException cnfe) {
            JOptionPane.showMessageDialog(null, "Driver Not Found!" + cnfe);
            System.exit(0);
        }
    }

    public static void main(String[] kon) {
        new Connections().dbConnection();
    }
}
